using namespace std;
#include <iostream>
#include <cstdlib>

struct node {
	int data;
	node* left;
	node* right;
};


int main() {
	node* root = NULL;
	for(int i=0; i<1000; i++) {
		double r = rand();
		root = Insert(root, r);
	}
	return 0;
}
node* Insert(node* root, int data); {
	if(*root == NULL) {
		*root = GetNewNode(data);
		return root;
}

node* GetNewNode(int data) {
	node* newNode = new node();
	newNode->data = data;
	newNode->left = newNode->right = NULL;
	return newNode;
}