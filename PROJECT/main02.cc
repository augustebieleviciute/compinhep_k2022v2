// main02.cc is a part of the PYTHIA event generator.
// Copyright (C) 2022 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage; z production; tevatron;

// This is a simple test program. It fits on one slide in a talk.
// It studies the pT_Z spectrum at the Tevatron.

#include "Pythia8/Pythia.h"
//#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TH1D.h"
#include "TMath.h"

using namespace Pythia8;
int main() {
  Pythia pythia;
  pythia.readString("Beams:idA = 2212");
//  pythia.readString("Beams:idB = 2212");
  pythia.readString("Beams:eCM = 14000.");
//  pythia.readString("HardQCD:all = on");
 //pythia.readString("SoftQCD:all = on");
 pythia.readString("25:m0 = 125");
 //pythia.readString("25:onMode = off");
 //pythia.readString("25:onIfMatch = 13 -13");
// pythia.readString("HiggsSM:all = on");
  //pythia.readString("HiggsSM:gg2Httbar = on");
 // pythia.readString("HiggsSM:ffbar2HZ = on");
  //pythia.readString("PhaseSpace:pTHatMin = 200.");
  //pythia.readString("Zprime:gmZmode = 4");
  pythia.readString("WeakSingleBoson:all = on");
 // pythia.readString("Top:all = on");
 


  double eta1, pt1, theta1, phi1, eta2, pt2, theta2, phi2, p1smear, p2smear;
  double theta1smear, theta2smear, phi1smear, phi2smear, pion1, pion2, invmass, p1cand, p2cand;
  double px1, py1, pz1, px2, py2, pz2, e1, e2, p1, p2;
  
  pythia.init();

  TFile *file = TFile::Open("dy.root", "recreate");
  TTree *T    = new TTree("T", "Tree");
  TCanvas *c  = new TCanvas();
  TRandom *rand = new TRandom();

  TH1D* imhistsig = new TH1D("imhistsig", "imhistsig", 100, 0, 300);
  
  T->Branch("eta1",        &eta1,   "eta/D");
  T->Branch("pt1",         &pt1,    "pt/D");
  T->Branch("theta1",      &theta1, "theta/D");
  T->Branch("phi1",        &phi1,   "phi/D");
  T->Branch("eta2",        &eta2,   "eta/D");
  T->Branch("pt2",         &pt2,    "pt/D");
  T->Branch("theta2",      &theta2, "theta/D");
  T->Branch("phi2",        &phi2,   "phi/D");
  T->Branch("p1smear",    &p1smear, "p1smear/D");
  T->Branch("p2smear",    &p2smear, "p2smear/D");
  T->Branch("theta1smear", &theta1smear, "theta1smear/D");
  T->Branch("theta2smear", &theta2smear, "theta2smear/D");
  T->Branch("phi1smear",   &phi1smear, "phi1smear/D");
  T->Branch("phi2smear",   &phi2smear, "phi2smear/D");
  T->Branch("pion1",       &pion1, "pion1smear/D");
  T->Branch("pion2",       &pion2, "pion2smear/D");
  T->Branch("invmass",     &invmass, "invmass/D");
  T->Branch("p1cand",     &p1cand, "p1cand/D");
  T->Branch("p2cand",     &p2cand, "p2cand/D");

  T->Branch("px1", &px1, "px1/D");
  T->Branch("py1", &py1, "py1/D");
  T->Branch("pz1", &pz1, "pz1/D");
  T->Branch("e1", &e1, "e1/D");

  T->Branch("px2", &px2, "px2/D");
  T->Branch("py2", &py2, "py2/D");
  T->Branch("pz2", &pz2, "pz2/D");
  T->Branch("e2", &e2, "e2/D");

  T->Branch("p1", &p1, "p1/D");
  T->Branch("p2", &p2, "p2/D");

  int counter = 0;

  for (int iEvent = 0; iEvent < 50000; ++iEvent) {
	  //std::cout << "BABAA1" << std::endl;
    if (!pythia.next()) continue;
    if ( iEvent % 100 == 0)
	    std::cout << "Event " << iEvent << std::endl;
	
	int muoncounter = 0, pioncounter = 0;
	int muonid = 0, pionid = 0;
	
    for (int i = 0; i < pythia.event.size(); ++i) {
	    //std::cout << muoncounter << std::endl;
		if (abs(pythia.event[i].id()) == 211) {
			if (pioncounter==2) {
				pion1 = (abs(pythia.event[pionid].pT()*pythia.event[pionid].pT()))-(abs(pythia.event[pionid].px()*pythia.event[pionid].px())+abs(pythia.event[pionid].py()*pythia.event[pionid].py())+abs(pythia.event[pionid].pz()*pythia.event[pionid].pz()));
				pion2 = abs(pythia.event[i].pT()*pythia.event[i].pT())-(abs(pythia.event[i].px()*pythia.event[i].px())+abs(pythia.event[i].py()*pythia.event[i].py())+abs(pythia.event[i].pz()*pythia.event[i].pz()));
				
			}
			pionid = i;
		}
      if (abs(pythia.event[i].id() == 13)){
		  ++counter;
		//std::cout << muoncounter << std::endl;
	      //std::cout << pythia.event[i].eta() << std::endl;
	      //std::cout << pythia.event[i].pT() << std::endl;
	      double px = abs(pythia.event[i].px()*pythia.event[i].px());
	      double py = abs(pythia.event[i].py()*pythia.event[i].py());
	      double pz = abs(pythia.event[i].pz()*pythia.event[i].pz());
	      double pt = abs(pythia.event[i].pT()*pythia.event[i].pT());

	      //std::cout << "pt " << pt << std::endl;

	      double p = sqrt(abs(pt-(px+py+pz)));

//	      std::cout << "p " << p << std::endl;
//	      std::cout << "eta " << abs(pythia.event[i].eta()) << std::endl;
		  
	      if(abs(pythia.event[i].eta()) < 2.1 && abs(p) > 20) {
		      ++muoncounter;
		     // std::cout << "PASSED" << std::endl;
			  
			  if (muoncounter == 2) {
				  std::cout << "TWO MUONS" << std::endl;
				  eta1   = pythia.event[muonid].eta();
				  pt1    = abs(pythia.event[muonid].pT());
				  theta1 = pythia.event[muonid].theta();
				  phi1   = pythia.event[muonid].phi();
				  px1    = abs(pythia.event[muonid].px());
				  py1    = abs(pythia.event[muonid].py());
				  pz1    = abs(pythia.event[muonid].pz());
				  
				  eta2   = pythia.event[i].eta();
				  pt2    = abs(pythia.event[i].pT());
				  theta2 = pythia.event[i].theta();
				  phi2   = pythia.event[i].phi();
				  px1    = abs(pythia.event[muonid].px());
				  py2    = abs(pythia.event[muonid].py());
				  pz2    = abs(pythia.event[muonid].pz());

				 p1 =  (pt1*pt1)-((px1*px1)+(py1*py1)+(pz1*pz1));
				 p2 =  (pt2*pt2)-((px2*px2)+(py2*py2)+(pz2*pz2));

				  //std::cout << "px1 " << px1 << std::endl;
				  
				  p1smear = p1 + (0.01*p1)*rand->Gaus();
				  p2smear = p2 + (0.01*p2)*rand->Gaus();
				  
				  theta1smear = theta1 + (0.012*theta1)*gRandom->Gaus();
				  theta2smear = theta2 + (0.012*theta2)*gRandom->Gaus();
				  phi1smear   = phi1 +   (0.012*phi1)*gRandom->Gaus();
				  phi2smear   = phi2 +   (0.012*phi2)*gRandom->Gaus();
				  
				  if (p1 > 30) {
					  p1cand = p1;
				  }
				  else {
					  p1cand = 0;
				  }
				  if (p2 > 30){
					  p2cand = p2;
				  }
				  else {
					  p2cand = 0;
				  }

				  //std::cout << "p1 " << (pt1*pt1)-((px1*px1)+(py1*py1)+(pz1*pz1)) << std::endl;
				  
				  invmass = sqrt(abs(((pt1*pt1)-((px1*px1)+(py1*py1)+(pz1*pz1)))+((pt2*pt2)-((px2*px2)+(py2*py2)+(pz2*pz2)))));
				  std::cout << "invmass " << invmass << std::endl;
				  
				  T->Fill();
				  imhistsig->Fill(invmass);
			  }
			  muonid = i;

  }
  }
  }
  }
  std::cout << counter << std::endl;
  pythia.stat();

  T->Print();
  T->Write();
  T->Scan();

  imhistsig->Draw();
  c->SaveAs("invmasshist.png");

  delete file;
  return 0;
 }
