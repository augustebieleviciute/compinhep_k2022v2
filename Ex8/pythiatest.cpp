#include "Pythia8/Pythia.h"
using namespace Pythia8;
int main() {
  // Generator. Process selection. Tevatron initialization. Histogram.
  Pythia pythia;
  pythia.readString("Beams:idB = -2212");
  pythia.readString("Beams:eCM = 1960.");
  //pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
  //pythia.readString("PhaseSpace:mHatMin = 80.");
  //pythia.readString("PhaseSpace:mHatMax = 120.");
  pythia.readString("HiggsSM:all = on");
  pythia.readString("25:m0 = 125i");
  
  pythia.init();
  Hist hig("Higgs width", 100, 0., 100.);
  // Begin event loop. Generate event. Skip if error. List first one.
  for (int iEvent = 0; iEvent < 1000; ++iEvent) {
    if (!pythia.next()) continue;
    // Loop over particles in event. Find last Z0 copy. Fill its pT.
    int iH = 0;
    for (int i = 0; i < pythia.event.size(); ++i)
      if (pythia.event[i].id() == 25) iH = i;
    hig.fill( pythia.event[iH].mWidth() );
  // End of event loop. Statistics. Histogram. Done.
  }
  pythia.stat();
  cout << hig;
  return 0;
}
