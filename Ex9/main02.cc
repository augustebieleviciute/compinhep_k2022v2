// main02.cc is a part of the PYTHIA event generator.
// Copyright (C) 2022 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage; z production; tevatron;

// This is a simple test program. It fits on one slide in a talk.
// It studies the pT_Z spectrum at the Tevatron.

#include "Pythia8/Pythia.h"
using namespace Pythia8;
int main() {
  // Generator. Process selection. Tevatron initialization. Histogram.
  Pythia pythia;
  pythia.readString("Beams:idB = -2212");
  pythia.readString("Beams:eCM = 13000.");
  pythia.readString("SoftQCD:all = on");
  //pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
  //pythia.readString("PhaseSpace:mHatMin = 80.");
  //pythia.readString("PhaseSpace:mHatMax = 120.");
  pythia.init();
  Hist pT("Transverse momentum", 100, 0., 200.);
  Hist eta("Pseudorapidity", 100, -5., 5.); 
  // Begin event loop. Generate event. Skip if error. List first one.
  for (int iEvent = 0; iEvent < 10000; ++iEvent) {
    if (!pythia.next()) continue;
    // Loop over particles in event. Find last Z0 copy. Fill its pT.
    int iZ = 0;
    for (int i = 0; i < pythia.event.size(); ++i)
      if (pythia.event[i].id() == 13) iZ = i;
    pT.fill( pythia.event[iZ].pT() );
    eta.fill( pythia.event[iZ].eta() );
  // End of event loop. Statistics. Histogram. Done.
  }
  pythia.stat();
  cout << pT << eta;
  return 0;
}
