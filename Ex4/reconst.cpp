#include <iostream>
#include <vector>
#include <math.h> 
#include <string>
using namespace std;

class Track
{
public:
	float fourmomentum[4];
	
	float getpt() {
		return sqrt(pow(fourmomentum[1],2)+pow(fourmomentum[2],2));
		
	}
	
	float getpseudo() {
		return asinh(fourmomentum[3]/getpt());
	}
};

class Particle : public Track
{
public:
	string particleID;
	string parentpID;
};

int main(){
	int dummy;
	/*
	Track track;
	track.fourmomentum[0] = 1;
	track.fourmomentum[1] = 3;
	track.fourmomentum[2] = 4;
	track.fourmomentum[3] = 2;
	cout << track.getpt() << endl; */
	
	Particle particle;
	particle.fourmomentum[0] = 7;
	particle.fourmomentum[1] = 8;
	particle.fourmomentum[2] = 2;
	particle.fourmomentum[3] = 4;
	particle.particleID = "muon";
	cout << particle.getpt() << endl;
	cout << particle.getpseudo() << endl;
	cout << particle.particleID << endl;
	cin >> dummy;
	return 0;
};