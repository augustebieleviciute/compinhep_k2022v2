#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;
class Measurement {
	public:
		double* xyval [2] = {};
		double* errMatrix [2][2];
		string measTextFile;
		string errTextFile;
		
		double getXY() {
			double* x, y;
			int dummy;
			ifstream f(measTextFile);
			f >> x >> y;

			xyval[0] = x;
			xyval[1] = y;
			cout << xyval[0] << " "<< xyval[1] << endl;
			//cin >> dummy;
			return xyval;
		}
		double significance() {
			double* x2, xy1, xy2, y2, sig;
			int dummy;
			ifstream f(errTextFile);
			f >> x2 >> xy1 >> xy2 >> y2;
			//cout << xyval[0];
			errMatrix[0][0] = x2;
			errMatrix[0][1] = xy1;
			errMatrix[1][0] = xy2;
			errMatrix[1][1] = y2;
			
			sig = (sqrt(x2)*xyval[0] + sqrt(y2)*xyval[1])/(pow(xyval[0],2) + pow(xyval[1],2));
			cout << sig << endl;
			cin >> dummy;
			return sig;
		}

};

int main() {
	Measurement obj;
	obj.measTextFile = "measurement.txt";
	obj.errTextFile = "error.txt";
	cout << obj.getXY() << endl;
	obj.significance();
};